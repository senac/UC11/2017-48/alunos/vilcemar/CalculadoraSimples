package br.com.senac.calculadorasimples;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final Button bntSoma = (Button)findViewById(R.id.soma);
        Button bntMult = (Button)findViewById(R.id.multiplicar);
        final Button bntDiv = (Button)findViewById(R.id.divisão);
        Button bntSub = (Button)findViewById(R.id.homeAsUp);


        //Converter


    private void resultValor(int n){

        final EditText Num01 = (EditText) findViewById(R.id.Num1);
        final EditText Num02 = (EditText) findViewById(R.id.Num2);

        final TextView Resultado = (TextView) findViewById(R.id.result);
        final TextView mensagValor = (TextView) findViewById(R.id.mensageValor);


            Double n1 = Double.parseDouble(Num01.getText().toString());
            Double n2 = Double.parseDouble(Num02.getText().toString());
            double result = 0;

            String valor = String.valueOf(result);

            switch (n){
                case 1: //somar
                    result = soma(n1,n2);
                    convert(n1,n2);

                    Resultado.setText(valor.toString());
                    mensagValor.setText(val1 + " + " + val2);
                    break;

                case 2: //dividir
                    result = dividir(n1,n2);
                    convert(n1,n2);
                    Resultado.setText(valor.toString());
                    mensagValor.setText(val1 + " / " + val2);

                    break;

                case 3: //multiplicar
                    result = multiplicar(n1,n2);

                    Resultado.setText(valor.toString());
                    mensagValor.setText(val1 + " x " + val2);

                    break;

                case 4: //subtrair
                    result = subtrair(n1,n2);
                    Resultado.setText(valor.toString());
                    mensagValor.setText(val1 + " - " + val2);

                    break;
            }








    }

    private void convert(Double n1, Double n2){
        String val1 = String.valueOf(n1);
        String val2 = String.valueOf(n2);
        return val1,val2;
        }
    }

        private void limparCampo(){
            //limpar
            final TextView Resultado = (TextView) findViewById(R.id.result);
            final TextView mensagValor = (TextView) findViewById(R.id.mensageValor);

            Resultado.setText("");
            mensagValor.setText("");
        }





    }


    private double soma(double a, double b){
        return a + b;
    }

    private double dividir(double a,double b){
        return a / b;
    }

    private double multiplicar(double a,double b){
        return a * b;
    }

    private double subtrair(double a,double b){
        return a - b;
    }



}


